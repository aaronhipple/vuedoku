import { reactive } from "vue";

export const store = {
  state: reactive({
    grid: [],
    focused: []
  }),
  setGrid(grid) {
    console.log("setting grid", grid);
    this.state.grid = grid;
  },
  setFocused(x, y) {
    this.state.focused = [{ x, y }];
  },
  setValue(n) {
    this.state.focused.forEach(({ x, y }) => {
      const previous = this.state.grid[y][x];
      if (previous && previous.readonly) return;
      this.state.grid[y][x] = { v: n };
    });
  },
  clearValue() {
    this.state.focused.forEach(({ x, y }) => {
      const previous = this.state.grid[y][x];
      if (previous && previous.readonly) return;
      this.state.grid[y][x] = null;
    });
  }
};
